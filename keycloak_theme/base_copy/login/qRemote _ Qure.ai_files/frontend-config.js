window._env_ = {
  SKIP_PREFLIGHT_CHECK: "true",
  API_URL: "https://staging.qure.ai:6010/",
  PROJECT_NAME: "qer",
  VAPID: "BGJBCWQaWm0mqq7VQXR0QImlzZ-Z2X3xyaXmC3DpqylnM-tU0QYM-a2eNl1Jo6ETcV_VZeCj5nSI0a8e9PP6J1c",
}
